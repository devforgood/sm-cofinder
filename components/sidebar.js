import { default as stylesMixin, template as styleTpl } from '../mixins/styles'
import authMixin from '../mixins/auth'

Vue.component('sidebar', {
    mixins: [stylesMixin, authMixin],
    props: ['logo'],
    template: styleTpl(`
    <div :class="sidebarClass" ref="root" v-show="isLogged">
        <div class="sidebar_menu">
            <img class="logo" :src="logo"/>
            <ul v-show="collapsed && isLogged">
                <li>
                    <button class="btn" @click="$router.push({name:'dashboard'})">Dashboard</button>
                </li>
                <li>
                    <button class="btn" @click="$router.push({name:'profile'})">Profile</button>
                </li>
            </ul>

            <div class="bottom">
            <i @click="toggle(false)" v-show="collapsed" class="fas fa-angle-double-left toggle_btn"></i>
            <i @click="toggle(true)" v-show="!collapsed" class="fas fa-angle-double-right toggle_btn"></i>
            </div>

        </div>
        <div class="sidebar_slot" v-show="isLogged">
            <slot  ></slot>
        </div>
    </div>
    </div>
    `),
    data() {
        return {
            styles: `
            .logo{
                max-width: 80%;
max-height: 100px;
margin: 0px auto;
    margin-top: 0px;
margin-top: 10px;
display: block;
width: 100%;
            }
            .bottom{
                position:absolute;
                bottom:5px;
                width:100%;
            }
            .sidebar_menu button{
                width:100%;
                text-align:left;

                background: transparent;
                max-width: 100%;
                border-radius: 0px;
                padding: 25px;
            }
            .sidebar_menu button:hover{
                background: #ffffff29;
                color: #fff;
            }
                .sidebar_slot{
                    overflow-y: auto;
                    max-height: calc(100vh - 100px);
                }
                ul{
                    list-style: none;
                    padding: 0px;
                }
                .sidebar li{
                    margin-bottom:0px;
                }
                .sidebar{
                    display: grid;
                    grid-template-columns: 50px 1fr;
                    grid-template-areas: 'sidebar content';
                    
                }
                .sidebar_menu{
                    position:relative;
                    width:auto;
                    height:calc(100vh);
                    background-color:#30426A;
                    margin-top:-100px;
                    grid-area: 'sidebar'
                }
                .sidebar.collapsed{
                    grid-template-columns: 230px 1fr;
                }
                .sidebar .toggle_btn{
                    cursor:pointer;
                    font-size:30px;
                    color:white;
                    text-align: center;
                    margin:20px auto;
                    display:block;
                }
            `,
            collapsed: true,
            userModuleId: '',
            resizeInterval: null,
            toggleColdown: null
        }
    },
    computed: {
        sidebarClass() {
            return `sidebar ${this.collapsed ? 'collapsed' : ''}`
        }
    },
    created() { },
    destroyed() {
        clearInterval(this.resizeInterval)
    },
    methods: {
        toggle(value) {
            this.collapsed = value
            this.toggleColdown = Date.now() + 1000 * 20
        }
    },
    async mounted() {
        this.resizeInterval = setInterval(() => {
            if (window.innerWidth < 768) {
                if (this.toggleColdown === null && this.collapsed) {
                    this.toggle(false)
                } else {
                    if (Date.now() > this.toggleColdown) {
                        this.toggle(false)
                    }
                }
            } else {

            }
        }, 1000)
        this.userModuleId = await api.funql({ name: 'getUserModuleId' })
    }
})