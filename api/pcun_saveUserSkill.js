module.exports = app => {
    var debug = require('debug')(`app:api:pcun_save ${`${Date.now()}`.white}`)
    return async function pcun_save(form) {
        let skill = await app.dbExecute(`SELECT id FROM skills WHERE LOWER(name) = LOWER(?)`, [form.name], {
            dbName: this.dbName,
            single: true
        })
        if (!skill) {
            debug('save skill', {
                form
            })
            var r = await app.pcun_saveDocument({
                name: form.name,
                _table: 'skills',
                _fields: ['name'],
                _options: {
                    dbName: this.dbName
                }
            })
            skill = {
                id: r.insertId
            }
        }
        return await app.dbExecute(`INSERT INTO 
        user_skills(user_id,skill_id)
        VALUES(?,?)
        ON DUPLICATE KEY UPDATE 
        skill_id = VALUES(skill_id)`, [this.pcunUser.id, skill.id
            ], {
                dbName: this.dbName
            })
    }
}