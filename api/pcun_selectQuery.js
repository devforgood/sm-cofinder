module.exports = app => {
    var debug = require('debug')(`app:api:selectQuery ${`${Date.now()}`.white}`)
    return async function selectQuery(data) {
        for (var x in data.queryArgs) {
            if (typeof data.queryArgs[x] === 'string') {
                data.queryArgs[x] = data.queryArgs[x].split('PCUNUSERID').join(this.pcunUser.id)
            }
        }
        return await app.dbExecute(
            `
SELECT ${data.query}
    `,
            data.queryArgs || [],
            Object.assign({
                dbName: this.dbName
            }, data.queryOptions || {})
        )
    }
}