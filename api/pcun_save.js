module.exports = app => {
    var debug = require('debug')(`app:api:pcun_save ${`${Date.now()}`.white}`)
    return async function pcun_save(form) {
        return app.pcun_saveDocument(Object.assign({}, form, {
            _options: {
                dbName: this.dbName
            }
        }))
    }
}