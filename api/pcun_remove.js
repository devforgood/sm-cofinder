module.exports = app => {
    var debug = require('debug')(`app:api:pcun_remove ${`${Date.now()}`.white}`)
    return async function pcun_remove(data) {
        debug('REMOVING', data)
        return await app.dbExecute(`DELETE FROM ${data.table} WHERE ${data.field} = ?`, [data.value],
            {
                dbName: this.dbName
            }
        )
    }
}