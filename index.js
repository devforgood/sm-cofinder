module.exports = async (app, config) => {
    const express = require('express')

    app.use(config.getRouteName('/static'), express.static(config.getPath('static')))

    app.loadFunctions({
        path: config.getPath('functions'),
        scope: {
            dbName: config.db_name
        }
    })

    app.loadApiFunctions({
        path: config.getPath('api'),
        middlewares: [async function (app) {
            if (this.req && this.req.user) {
                if (this.req.session.pcunUserId) {
                    this.req.user.pcunUser = await app.dbExecute(`SELECT * FROM users WHERE id = ?`, [this.req.session.pcunUserId], { single: true, dbName: this.dbName })
                } else {
                    this.req.user.pcunUser = await app.dbExecute(`SELECT * FROM users WHERE email = ?`, [this.req.user.email], { single: true, dbName: this.dbName })
                    
                    if(!this.req.user.pcunUser){
                        await app.api.pcun_registerUser({
                            email: this.req.user.email
                        })
                        this.req.user.pcunUser = await app.dbExecute(`SELECT * FROM users WHERE email = ?`, [this.req.user.email], { single: true, dbName: this.dbName })
                    }
                    
                    this.req.session.pcunUserId = this.req.user.pcunUser.id
                }
                this.pcunUser = this.req.user.pcunUser
            }
            //console.log('PCUN USER ID IS ', this.req.user.pcunUser)
        }],
        scope: ({ req }) => {
            let dbName = config.db_name
            return {
                moduleId: config.id,
                dbName
            }
        }
    })

    app.get(
        config.getRouteName('app.js'),
        app.webpackMiddleware({
            entry: config.getPath('app.js'),
            output: config.getPath('tmp/app.js')
        })
    )

    app.get(
        config.getRouteName('/'),
        app.builder.transformFileRoute({
            cwd: config.getPath(),
            source: 'app.pug',
            mode: 'pug',
            transform: [app.cacheCDNScripts],
            context: {
                cwd: config.getRouteName(),
                head: {
                    title: config.title
                }
            }
        })
    )
}